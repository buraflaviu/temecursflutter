import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String _title = "Currency Converter";

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final TextEditingController controllerForText = TextEditingController();

  bool isTextValid = true;
  double? convertedText;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: MyApp._title,
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(title: const Text(MyApp._title)),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.network(
                "https://media.cancan.ro/unsafe/1260x709/smart/filters:contrast(5):format(webp):quality(80)/http://cancan.ro/wp-content/uploads/2022/03/bani.jpg"),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controllerForText,
                cursorColor: const TextSelectionThemeData().cursorColor,
                decoration: InputDecoration(
                  hintText: 'enter the amount in EUR',
                  errorText: isTextValid ? null : 'please enter a number',
                  enabledBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                ),
              ),
            ),
            OutlinedButton(
              onPressed: () {
                setState(() {
                  try {
                    convertedText =
                        double.parse(controllerForText.text.toString()) * 4.5;
                    isTextValid = true;
                  } catch (e) {
                    isTextValid = false;
                  }
                });
              },
              child: const Text("Convert"),
            ),
            Text(
              (convertedText == null) || (isTextValid == false)
                  ? ''
                  : "${convertedText!.toStringAsFixed(2)} RON",
              style: const TextStyle(fontSize: 50, color: Colors.blueGrey),
            )
          ],
        ),
      ),
    );
  }
}
